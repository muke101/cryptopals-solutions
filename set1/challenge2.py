def xor(hexString):
    hexBytes = bytes.fromhex(hexString)
    hexConst = bytes.fromhex('686974207468652062756c6c277320657965') 
    res = bytearray()
    
    for b1, b2 in zip(hexBytes, hexConst):
        res.append(b1 ^ b2)

    return res.hex()
    
print(xor('1c0111001f010100061a024b53535009181c'))
