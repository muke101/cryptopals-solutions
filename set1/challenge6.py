import base64
import operator
import collections
import itertools

def hamming_distance(code1, code2):
    diffs = 0
    for c1, c2 in zip(code1, code2):
        while c1 or c2:
            if c1 & 1 != c2 & 1:
                diffs+=1
            c1 >>= 1
            c2 >>= 1
    return diffs

def get_keysize(hex_bytes, guesses):
    distances = {}
    for keysize in range(2,guesses+1):
        distance = 0
        block1 = hex_bytes[:keysize] 
        block2 = hex_bytes[keysize:keysize*2]
        block3 = hex_bytes[keysize*2:keysize*3]
        block4 = hex_bytes[keysize*3:keysize*4]
        pairs = itertools.combinations([block1,block2,block3,block4],2)
        for (c1,c2) in pairs:
            distance+=hamming_distance(c1,c2)
        distance /= 6
        distance /= keysize
        distances[keysize] = distance
    mins = []
    for i in range(4):
        currentMin = min(distances.items(), key=operator.itemgetter(1))[0]
        mins.append(currentMin)
        del distances[currentMin]
    return mins


def transpose(hex_bytes, keysize):
    blocks = []
    for block in range(keysize):
        blocks.append([])

    block_number = len(hex_bytes)//keysize
    remainder = len(hex_bytes)%keysize

    for i in range(block_number):
        block = hex_bytes[i*keysize:(i*keysize)+keysize]
        for j in range(keysize):
            blocks[j].append(block[j])

    if remainder:
        block = hex_bytes[-remainder:]
        for i in range(remainder):
            blocks[i].append(block[i])

    return blocks

def xor(hex_bytes, key):
    plaintext = []
    for b in hex_bytes:
        plaintext.append(chr(b ^ key))
    return plaintext

def freq_analysis(text,key):
    scores = {}
    expected_freqs = {
        'a': 0.0651738, 'b': 0.0124248, 'c': 0.0217339, 'd': 0.0349835, 'e': 0.1041442, 'f': 0.0197881, 'g': 0.0158610,
        'h': 0.0492888, 'i': 0.0558094, 'j': 0.0009033, 'k': 0.0050529, 'l': 0.0331490, 'm': 0.0202124, 'n': 0.0564513,
        'o': 0.0596302, 'p': 0.0137645, 'q': 0.0008606, 'r': 0.0497563, 's': 0.0515760, 't': 0.0729357, 'u': 0.0225134,
        'v': 0.0082903, 'w': 0.0171272, 'x': 0.0013692, 'y': 0.0145984, 'z': 0.0007836, ' ': 0.1918182
    }
    
    score = 0 

    for c in text:
        score += expected_freqs.get(c, 0)

    result = {
        "score": score,
        "key": key,
        "plaintext": text
    }

    return result

def decrypt(hex_bytes):
    candidates = []

    for c in range(0,128):
        plaintext = xor(hex_bytes,c)
        candidates.append(freq_analysis(plaintext,c))

    return sorted(candidates,key=lambda c: c['score'], reverse=True)[0]

def repeatingXor(msg, key):
    length = len(msg)//len(key)
    remainder = len(msg)%len(key)
    remainderKey = key[:remainder]

    key = key*length
    if remainder:
        key+=remainderKey

    enc = bytearray()

    for m, k in zip(msg, key):
        enc.append(m ^ ord(k))

    res = []

    for c in enc:
        res.append(chr(c))

    return ''.join(res)

data = base64.b64decode(open('repeatingxor', 'r').read())

keysizes = get_keysize(data, 40)

for keysize in keysizes[:1]:
    key = []
    for block in transpose(data, keysize):
        candidate = decrypt(block)
        key.append(chr(candidate['key']))
    print(repeatingXor(data,''.join(key)))
