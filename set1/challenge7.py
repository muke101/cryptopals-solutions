from Crypto.Cipher import AES
import base64

key = b'YELLOW SUBMARINE'
cipher = AES.new(key, AES.MODE_ECB)

data = base64.b64decode(open("aes-ecb","r").read())

ciphertext = cipher.decrypt(data)
print(ciphertext.decode())
