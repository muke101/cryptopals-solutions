import binascii 

def repeatingXor(msg, key):
    length = len(msg)//len(key)
    remainder = len(msg)%len(key)
    remainderKey = key[:remainder]

    key = key*length
    if remainder:
        key+=remainderKey

    enc = bytearray()

    for m, k in zip(msg, key):
        enc.append(m ^ ord(k))

    return enc.hex()
        
        

msg = b"Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"

print(repeatingXor(msg, "ICE"))
