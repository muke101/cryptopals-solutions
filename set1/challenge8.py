from Crypto.Cipher import AES
import collections
import operator

def split_blocks(hex_bytes, block_size):
    blocks = collections.defaultdict(int)
    
    for i in range(len(hex_bytes)//block_size):
        block = tuple(hex_bytes[i*block_size:][j] for j in range(block_size))
        blocks[block] += 1
    
    highest = max(blocks.items(), key=operator.itemgetter(1))[1]
    
    return highest

hex_codes = [bytes.fromhex(line.strip()) for line in open("hex-ciphertexts", "r")]

scores = {}

for i in range(len(hex_codes)):
    scores[i] = split_blocks(hex_codes[i],128/8)

position = max(scores.items(),key=operator.itemgetter(1))[0]
ecb_encrypted = hex_codes[position]

print(ecb_encrypted)
