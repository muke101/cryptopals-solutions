import collections
import operator
import binascii

def xor(hex_bytes, key):
    plaintext = []
    for b in hex_bytes:
       plaintext.append(chr(b ^ key))
    return plaintext

def freq_analysis(text):
    expected_freqs = {
        'a': 0.0651738, 'b': 0.0124248, 'c': 0.0217339, 'd': 0.0349835, 'e': 0.1041442, 'f': 0.0197881, 'g': 0.0158610,
        'h': 0.0492888, 'i': 0.0558094, 'j': 0.0009033, 'k': 0.0050529, 'l': 0.0331490, 'm': 0.0202124, 'n': 0.0564513,
        'o': 0.0596302, 'p': 0.0137645, 'q': 0.0008606, 'r': 0.0497563, 's': 0.0515760, 't': 0.0729357, 'u': 0.0225134,
        'v': 0.0082903, 'w': 0.0171272, 'x': 0.0013692, 'y': 0.0145984, 'z': 0.0007836, ' ': 0.1918182
    }

    score = 0

    for c in text:
        score += expected_freqs.get(c,0)

    result = {
        "score": score,
        "plaintext": ''.join(text)
    }

    return result

def test_hex(hex_bytes):
    candidates = []
    
    for c in range(0, 128):
        plaintext = xor(hex_bytes, c)
        candidates.append(freq_analysis(plaintext))

    return sorted(candidates,key=lambda c: c['score'], reverse=True)[0]

hexes = open("hexstrings", "r").readlines() 
candidates = []

for hex_bytes in hexes:
    hex_bytes = bytes.fromhex(hex_bytes.strip())
    candidates.append(test_hex(hex_bytes))


cand = sorted(candidates, key=lambda c: c['score'], reverse=True)[0]

print(cand['plaintext'])
